# https://www.youtube.com/watch?v=O3b8lVF93jU&t=368s

import cv2

vid = cv2.VideoCapture(0)
object_detector = cv2.createBackgroundSubtractorMOG2()

while(True):
    # Capture the video frame
    ret, frame = vid.read()
  
    mask = object_detector.apply(frame)
    # Display the resulting frame


    _, mask = cv2.threshold(mask, 100,255, cv2.THRESH_BINARY )
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    for cnt in contours:
        # Calculate area and remove small elements
        area = cv2.contourArea(cnt)
        if area > 100:
            cv2.drawContours(mask, [cnt], -1, (0, 255, 0), 2)

    cv2.imshow('frame', mask)
    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
  
# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()